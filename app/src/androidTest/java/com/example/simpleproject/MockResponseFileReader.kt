package com.example.simpleproject

import junit.framework.Assert.assertEquals
import org.junit.Test
import java.io.InputStreamReader

class MockResponseFileReader() {
    val content: String
    var path: String = "test.json"
    init {
        val reader = InputStreamReader(this.javaClass.classLoader?.getResourceAsStream(path))
        content = reader.readText()
        reader.close()
    }

}