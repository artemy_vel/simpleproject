package com.example.simpleproject

import androidx.test.core.app.launchActivity

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.ViewAction
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.runners.AndroidJUnit4
import org.junit.Test
import org.junit.runner.RunWith

/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4::class)
class UiTest {
    @Test
    fun testEvent() {
        launchActivity<MainActivity>()
        onView(withId(R.id.btn_calculate)).perform(click())
    }

    @Test
    fun testBasicInput() {
        launchActivity<MainActivity>()
        onView(withId(R.id.edt_concurrency_number))
            .perform(typeText("56"), closeSoftKeyboard())
        onView(withId(R.id.btn_calculate)).perform(click())
    }

}


