package com.example.simpleproject

import com.example.simpleproject.newtwork.Rate
import com.example.simpleproject.repository.ConcurrencyRepository
import junit.framework.Assert.assertEquals
import junit.framework.Assert.assertNotNull
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.junit.After
import org.junit.Before
import org.junit.Test

class RetrofitTest {

    lateinit var server: MockWebServer
    private val testjob = Job()
    private val coroutineScope = CoroutineScope(
        testjob + Dispatchers.Main
    )

    @Before
    fun initTest() {
        server = MockWebServer()
    }

    @After
    fun shutdown() {
        server.shutdown()
    }

    @Test
    fun isBodyCorrect() {
        server.apply {
            enqueue(MockResponse().setBody(MockResponseFileReader().content))
        }
        coroutineScope.launch {
            assertNotNull(
                ConcurrencyRepository().getConcurrency().await().rates
            )
        }
    }
}