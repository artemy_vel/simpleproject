package com.example.simpleproject.newtwork

data class Rate(
    var EUR: String,
    var USD: String
)