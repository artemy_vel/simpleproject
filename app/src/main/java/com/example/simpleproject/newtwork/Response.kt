package com.example.simpleproject.newtwork

data class Response(
    val rates: Rate? = null,
    val base: String? = null,
    val date: String? = null
)
