package com.example.simpleproject.viewmodel


import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.simpleproject.newtwork.Rate
import com.example.simpleproject.repository.ConcurrencyRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

class ConcurrencyViewModel : ViewModel() {
    private val repository: ConcurrencyRepository = ConcurrencyRepository()

    private var viewModelJob = Job()
    private val coroutineScope = CoroutineScope(
        viewModelJob + Dispatchers.Main
    )
    val concurrency = MutableLiveData<Rate>()

    fun getConcurrency() {
        coroutineScope.launch {
            concurrency.postValue(repository.getConcurrency().await().rates)
        }
    }
}