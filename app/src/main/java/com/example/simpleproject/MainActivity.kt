package com.example.simpleproject

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.example.simpleproject.newtwork.Rate
import com.example.simpleproject.viewmodel.ConcurrencyViewModel
import kotlinx.android.synthetic.main.activity_main.*
import java.math.BigDecimal


class MainActivity : AppCompatActivity() {
    private lateinit var model: ConcurrencyViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        model = ViewModelProvider(this).get(ConcurrencyViewModel::class.java)
        model.concurrency.observe(this, androidx.lifecycle.Observer { concurrency ->
            updateUi(calculateConcurrency(concurrency))
        })
        btn_calculate.setOnClickListener {
            if (edt_concurrency_number.text.toString() != "")
                getExchangeRate()
        }
    }

    fun getExchangeRate() {
        model.getConcurrency()
    }

    fun updateUi(concurrency: Rate) {
        txv_dollar.text = getString(R.string.dollar_string, concurrency.USD)
        txv_euro.text = getString(R.string.euro_string, concurrency.EUR)
    }
    fun calculateConcurrency(concurrency: Rate): Rate {
        val dollar: BigDecimal =
            concurrency.USD.toBigDecimal() * edt_concurrency_number.text.toString().toBigDecimal()
        val euro: BigDecimal =
            concurrency.EUR.toBigDecimal() * edt_concurrency_number.text.toString().toBigDecimal()
        val dollarString: String = String.format("%.3f", dollar)
        val euroString: String = String.format("%.3f", euro)
        return Rate(euroString, dollarString)
    }


}