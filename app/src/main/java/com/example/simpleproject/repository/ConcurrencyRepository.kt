package com.example.simpleproject.repository

import com.example.simpleproject.newtwork.ConcurrencyApi
import com.example.simpleproject.newtwork.Response
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job


class ConcurrencyRepository() {
    fun getConcurrency(): Deferred<Response> {
        return ConcurrencyApi.retrofitService.getExchangeRate()
    }

}